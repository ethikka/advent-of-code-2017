﻿using System;
using System.Collections.Generic;

namespace Day_20
{
    class Program
    {
        private static bool Part_1 = false;

        static void Main(string[] args)
        {
            SortedDictionary<uint, uint> sortedRules = new SortedDictionary<uint, uint>();
            foreach (var s in Input.Solution())
                sortedRules.Add(Convert.ToUInt32(s.Split('-')[0]), Convert.ToUInt32(s.Split('-')[1]));

            uint total = 0, res = 0;
            foreach (var k in sortedRules.Keys)
            {
                if (res != uint.MaxValue)
                {
                    if ((res >= k) && (res <= sortedRules[k])) res = sortedRules[k] + 1;

                    if (res < k)
                    {
                        if (Part_1)
                        {
                            Console.WriteLine("ALLOWED IP: " + res.ToString());
                            return;
                        }

                        total += (k - res);
                        res = (sortedRules[k] != uint.MaxValue) ? res = sortedRules[k] + 1 : uint.MaxValue;
                    }
                }
            }
            total += (uint.MaxValue - res);
            Console.WriteLine("NUMBER OF ALLOWED IPS: " + total.ToString());
        }
    }
}
