﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Day_14
{
  public class HashCache
  {
    private string _salt = "abc";
    //private string _salt = "ahsbgdzn";
    private MD5CryptoServiceProvider _md5 = new MD5CryptoServiceProvider();
    private Dictionary<int, string> _hashes = new Dictionary<int, string>();

    private string hash(int idx)
    {
      // part B
      //string hash = BitConverter.ToString(_md5.ComputeHash(Encoding.ASCII.GetBytes(String.Format("{1}{0}", idx.ToString(), _salt)))).Replace("-", string.Empty).ToLower();
      //for (int i = 0; i < 2016; i++)
      //  hash = BitConverter.ToString(_md5.ComputeHash(Encoding.ASCII.GetBytes(hash))).Replace("-", string.Empty).ToLower();
      //return hash;

      // part A
      return BitConverter.ToString(_md5.ComputeHash(Encoding.ASCII.GetBytes(String.Format("{1}{0}", idx.ToString(), _salt)))).Replace("-", string.Empty).ToLower();
    }

    public string this[int idx]
    {
      get
      {
        if (!_hashes.ContainsKey(idx))
          _hashes[idx] = hash(idx);
        return _hashes[idx];
      }
    }
  }

  public class PadMeBabyOneMoreTime
  {
    private HashCache _hashCache = new HashCache();


    private bool isValidKey(int idx)
    {
      string hash = _hashCache[idx];
      for (int i = 0; i < hash.Length - 2; i++)
        if (hash[i] == hash[i + 1] && hash[i] == hash[i + 2])
        {
          int hitcount = 0;
          for (int nxt = 1; nxt <= 1000; nxt++)
            if (_hashCache[idx + nxt].Contains(new string(hash[i], 5)))
              hitcount++;
          return (hitcount == 1);
        } 
      return false;
    }

    List<int> hits = new List<int>();

    public int PadIndex(int num)
    {
      int idx = 0;
      while (hits.Count < 72)
      {
        if (isValidKey(idx))
          hits.Add(idx);
        idx++;
      }
      return hits[63];
    }
  }

  class Program
  {
    static void Main(string[] args)
    {
      Console.WriteLine(new PadMeBabyOneMoreTime().PadIndex(64).ToString());
      Console.ReadKey();
    }
  }
}
