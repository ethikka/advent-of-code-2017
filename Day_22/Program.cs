﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day_22
{
    public class NodeInfo
    {
        public int Size;
        public int Used;
        public int Available;
        // public int Use%??
    }

    public class TerseGrid<T> where T : class
    {
        Dictionary<string, T> _gridSpots = new Dictionary<string, T>();

        private string MakeKey(int x, int y)
        {
            return String.Format("{0} {1}", x, y);
        }

        public List<T> Items
        {
            get { return _gridSpots.Values.ToList(); }
        }

        public T this[int x, int y]
        {
            get
            {
                var key = MakeKey(x, y);
                if (_gridSpots.ContainsKey(key))
                    return _gridSpots[key];
                return null;
            }
            set { _gridSpots[String.Format("{0} {1}", x, y)] = value; }
        }
    }


    public class GridComputing
    {
        private TerseGrid<NodeInfo> _grid = new TerseGrid<NodeInfo>();

        private void PrepareGrid()
        {
            var input = Input.df();

            for (int i = 2; i < input.Length; i++)
            {
                var line = input[i].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                var newentry = new NodeInfo();
                newentry.Size = Convert.ToInt32(line[1].Replace("T", ""));
                newentry.Used = Convert.ToInt32(line[2].Replace("T", ""));
                newentry.Available = Convert.ToInt32(line[3].Replace("T", ""));
                var nod = line[0].Split('-');
                int x = Convert.ToInt32(nod[1].Replace("x", ""));
                int y = Convert.ToInt32(nod[2].Replace("y", ""));
                _grid[x, y] = newentry;
            }
        }


        public void Solve()
        {
            PrepareGrid();

            for (int i = 0; i < 36; i++)
                for (int j = 0; j < 36; j++)
                {
                    Console.SetCursorPosition(i, j);
                    if (i == 0 && j == 0)
                        Console.WriteLine("G");
                    else if (i == 29 && j == 0)
                        Console.WriteLine("T");
                    else
                    {
                      
                        var s = _grid[i, j];
                        if (s != null)
                            if (s.Used > 92)
                                Console.WriteLine("#");
                            else
                                if (s.Used == 0)
                                Console.WriteLine("_");
                            else Console.WriteLine(".");

                    }
                }


            return;
            // Part A
            int total = 0;
            var items = _grid.Items;
            for (int i = 0; i < items.Count; i++)
                for (int j = i + 1; j < items.Count; j++)
                {
                    if (_grid.Items[i].Used > 0 && (_grid.Items[j].Available > _grid.Items[i].Used))
                        total++;
                    if (_grid.Items[j].Used > 0 && (_grid.Items[i].Available > _grid.Items[j].Used))
                        total++;
                }

            Console.WriteLine("TOTAL VIABLE PAIRS: " + total.ToString());
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            new GridComputing().Solve();
            Console.ReadKey();
        }
    }
}
