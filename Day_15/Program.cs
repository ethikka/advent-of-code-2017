﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day_15
{
    /*
    Disc #1 has 13 positions; at time=0, it is at position 10.
    Disc #2 has 17 positions; at time=0, it is at position 15.
    Disc #3 has 19 positions; at time=0, it is at position 17.
    Disc #4 has 7 positions; at time=0, it is at position 1.
    Disc #5 has 5 positions; at time=0, it is at position 0.
    Disc #6 has 3 positions; at time=0, it is at position 1.
    */
    //             ((t+startpos)%19)-discNum
    public class TimingIsEverything
    {
        Dictionary<string, Func<int, bool>> formulas = new Dictionary<string, Func<int, bool>>();

        public TimingIsEverything()
        {
            /* Test input
            formulas.Add("Disc #1", (i) => { return (((i + 4) % 5) + 1) % 5 == 0; });
            formulas.Add("Disc #2", (i) => { return (((i + 1) % 2) + 2) % 2 == 0; });
            */

            /* My input */
            formulas.Add("Disc #3", (i) => { return (((i + 17) % 19) + 3) % 19 == 0; });
            formulas.Add("Disc #2", (i) => { return (((i + 15) % 17) + 2) % 17 == 0; });
            formulas.Add("Disc #1", (i) => { return (((i + 10) % 13) + 1) % 13 == 0; });
            // B solution
            formulas.Add("Disc #7", (i) => { return (((i +  0) % 11) + 7) % 11 == 0; });
            //
            formulas.Add("Disc #4", (i) => { return (((i +  1) %  7) + 4) %  7 == 0; });
            formulas.Add("Disc #5", (i) => { return (((i +  0) %  5) + 5) %  5 == 0; });
            formulas.Add("Disc #6", (i) => { return (((i +  1) %  3) + 6) %  3 == 0; });

            /* */
        }

        public bool GetCapsule(int t)
        {
            foreach(var f in formulas.Keys)
                if (!formulas[f](t))
                    return false;
            return true;
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            TimingIsEverything timings = new TimingIsEverything();
            int i = 0;
            while (true)
            {
                if (timings.GetCapsule(i))
                    break;
                i++;
            }
            Console.WriteLine(String.Format("You get a capsule if you press at T = {0}", i));
            Console.ReadKey();
        }
    }
}
