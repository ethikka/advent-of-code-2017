﻿using System;
using System.Collections.Generic;

namespace Day_1
{
    public static class ExtMeths
    {
        public static IEnumerable<int> To(this int from, int to)
        {
            if (from < to)
            {
                while (from <= to)
                {
                    yield return from++;
                }
            }
            else
            {
                while (from >= to)
                {
                    yield return from--;
                }
            }
        }

    }

    public class Position
    {
        public int X;
        public int Y;
    }

    enum Facing
    {
        north = 0,
        east,
        south,
        west
    }

    public class Day_1
    {

        Position _currentPosition = new Position();
        Dictionary<int, Dictionary<int, int>> _visitedpositions = new Dictionary<int, Dictionary<int, int>>();

        private Position Move(Facing heading, int steps)
        {
            int deltaX = 0;
            int deltaY = 0;
            switch (heading)
            {
                case Facing.north: deltaY = steps; break;
                case Facing.east: deltaX = steps; break;
                case Facing.south: deltaY = -1 * steps; break;
                case Facing.west: deltaX = -1 * steps; break;
            }

            _currentPosition.X += deltaX;
            _currentPosition.Y += deltaY;
            return null;
        }

        public void Execute()
        {
            var _steps = "R1, L3, R5, R5, R5, L4, R5, R1, R2, L1, L1, R5, R1, L3, L5, L2, R4, L1, R4, R5, L3, R5, L1, R3, L5, R1, L2, R1, L5, L1, R1, R4, R1, L1, L3, R3, R5, L3, R4, L4, R5, L5, L1, L2, R4, R3, R3, L185, R3, R4, L5, L4, R48, R1, R2, L1, R1, L4, L4, R77, R5, L2, R192, R2, R5, L4, L5, L3, R2, L4, R1, L5, R5, R4, R1, R2, L3, R4, R4, L2, L4, L3, R5, R4, L2, L1, L3, R1, R5, R5, R2, L5, L2, L3, L4, R2, R1, L4, L1, R1, R5, R3, R3, R4, L1, L4, R1, L2, R3, L3, L2, L1, L2, L2, L1, L2, R3, R1, L4, R1, L1, L4, R1, L2, L5, R3, L5, L2, L2, L3, R1, L4, R1, R1, R2, L1, L4, L4, R2, R2, R2, R2, R5, R1, L1, L4, L5, R2, R4, L3, L5, R2, R3, L4, L1, R2, R3, R5, L2, L3, R3, R1, R3".Split(',');

            int i = 0;
            Facing _look = Facing.north;

            while (i < _steps.Length)
            {
                switch (_steps[i].Trim()[0])
                {
                    case 'L': _look = (Facing)(((int)_look + 3) % 4); break;
                    case 'R': _look = (Facing)(((int)_look + 1) % 4); break;
                }

                var shortcut = Move(_look, Convert.ToInt32(_steps[i].Trim().Substring(1)));
                if (shortcut != null)
                    Console.WriteLine(String.Format("x:{0} y:{1} distance:{2}", shortcut.X, shortcut.Y, (Math.Abs(shortcut.X) + Math.Abs(shortcut.Y))));
                i++;
            }
            Console.WriteLine(String.Format("x:{0} y:{1} distance:{2}", _currentPosition.X, _currentPosition.Y, (Math.Abs(_currentPosition.X) + Math.Abs(_currentPosition.Y))));
        }
    }

    public class Day_1b 
    {
        Position _currentPosition = new Position();
        List<string> _visitedpositions = new List<string>();

        private Position Move(Facing heading, int steps)
        {
            int deltaX = 0;
            int deltaY = 0;
            switch (heading)
            {
                case Facing.north: deltaY = steps; break;
                case Facing.east: deltaX = steps; break;
                case Facing.south: deltaY = -1 * steps; break;
                case Facing.west: deltaX = -1 * steps; break;
            }

            bool skip = true;

            foreach (int x in _currentPosition.X.To(_currentPosition.X + deltaX))
                foreach (int y in _currentPosition.Y.To(_currentPosition.Y + deltaY))
                {
                    if (!skip)
                    {
                        if (_visitedpositions.Contains(String.Format("{0}#{1}", x, y)))
                            return new Position() { X = x, Y = y };
                        _visitedpositions.Add(String.Format("{0}#{1}", x, y));
                    }
                    skip = false;
                }

            _currentPosition.X += deltaX;
            _currentPosition.Y += deltaY;
            return null;
        }

        public void Solve()
        {
            var _steps = "R1, L3, R5, R5, R5, L4, R5, R1, R2, L1, L1, R5, R1, L3, L5, L2, R4, L1, R4, R5, L3, R5, L1, R3, L5, R1, L2, R1, L5, L1, R1, R4, R1, L1, L3, R3, R5, L3, R4, L4, R5, L5, L1, L2, R4, R3, R3, L185, R3, R4, L5, L4, R48, R1, R2, L1, R1, L4, L4, R77, R5, L2, R192, R2, R5, L4, L5, L3, R2, L4, R1, L5, R5, R4, R1, R2, L3, R4, R4, L2, L4, L3, R5, R4, L2, L1, L3, R1, R5, R5, R2, L5, L2, L3, L4, R2, R1, L4, L1, R1, R5, R3, R3, R4, L1, L4, R1, L2, R3, L3, L2, L1, L2, L2, L1, L2, R3, R1, L4, R1, L1, L4, R1, L2, L5, R3, L5, L2, L2, L3, R1, L4, R1, R1, R2, L1, L4, L4, R2, R2, R2, R2, R5, R1, L1, L4, L5, R2, R4, L3, L5, R2, R3, L4, L1, R2, R3, R5, L2, L3, R3, R1, R3".Day1().Split(',');

            int i = 0;
            Facing _look = Facing.north;
            while (i < _steps.Length)
            {
                switch (_steps[i].Trim()[0])
                {
                    case 'L': _look = (Facing)(((int)_look + 3) % 4); break;
                    case 'R': _look = (Facing)(((int)_look + 1) % 4); break;
                }

                var shortcut = Move(_look, Convert.ToInt32(_steps[i].Trim().Substring(1)));
                if (shortcut != null)
                    Console.WriteLine(String.Format("x:{0} y:{1} distance:{2}", shortcut.X, shortcut.Y, (Math.Abs(shortcut.X) + Math.Abs(shortcut.Y))));
                i++;
            }
            Console.WriteLine(String.Format("x:{0} y:{1} distance:{2}", _currentPosition.X, _currentPosition.Y, (Math.Abs(_currentPosition.X) + Math.Abs(_currentPosition.Y))));
        }
    }
}


class Program
{
    static void Main(string[] args)
    {
        new Day_1.Day_1().Solve();
        new Day_1.Day_1b().Solve();
    }
}
}
