﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day_21
{
    public class ScrambledLettersAndHash
    {
        private string[] _instructions = new[] { "rotate right 3 steps", "swap letter b with letter a", "move position 3 to position 4", "swap position 0 with position 7", "swap letter f with letter h", "rotate based on position of letter f", "rotate based on position of letter b", "swap position 3 with position 0", "swap position 6 with position 1", "move position 4 to position 0", "rotate based on position of letter d", "swap letter d with letter h", "reverse positions 5 through 6", "rotate based on position of letter h", "reverse positions 4 through 5", "move position 3 to position 6", "rotate based on position of letter e", "rotate based on position of letter c", "rotate right 2 steps", "reverse positions 5 through 6", "rotate right 3 steps", "rotate based on position of letter b", "rotate right 5 steps", "swap position 5 with position 6", "move position 6 to position 4", "rotate left 0 steps", "swap position 3 with position 5", "move position 4 to position 7", "reverse positions 0 through 7", "rotate left 4 steps", "rotate based on position of letter d", "rotate left 3 steps", "swap position 0 with position 7", "rotate based on position of letter e", "swap letter e with letter a", "rotate based on position of letter c", "swap position 3 with position 2", "rotate based on position of letter d", "reverse positions 2 through 4", "rotate based on position of letter g", "move position 3 to position 0", "move position 3 to position 5", "swap letter b with letter d", "reverse positions 1 through 5", "reverse positions 0 through 1", "rotate based on position of letter a", "reverse positions 2 through 5", "swap position 1 with position 6", "swap letter f with letter e", "swap position 5 with position 1", "rotate based on position of letter a", "move position 1 to position 6", "swap letter e with letter d", "reverse positions 4 through 7", "swap position 7 with position 5", "swap letter c with letter g", "swap letter e with letter g", "rotate left 4 steps", "swap letter c with letter a", "rotate left 0 steps", "swap position 0 with position 1", "reverse positions 1 through 4", "rotate based on position of letter d", "swap position 4 with position 2", "rotate right 0 steps", "swap position 1 with position 0", "swap letter c with letter a", "swap position 7 with position 3", "swap letter a with letter f", "reverse positions 3 through 7", "rotate right 1 step", "swap letter h with letter c", "move position 1 to position 3", "swap position 4 with position 2", "rotate based on position of letter b", "reverse positions 5 through 6", "move position 5 to position 3", "swap letter b with letter g", "rotate right 6 steps", "reverse positions 6 through 7", "swap position 2 with position 5", "rotate based on position of letter e", "swap position 1 with position 7", "swap position 1 with position 5", "reverse positions 2 through 7", "reverse positions 5 through 7", "rotate left 3 steps", "rotate based on position of letter b", "rotate left 3 steps", "swap letter e with letter c", "rotate based on position of letter a", "swap letter f with letter a", "swap position 0 with position 6", "swap position 4 with position 7", "reverse positions 0 through 5", "reverse positions 3 through 5", "swap letter d with letter e", "move position 0 to position 7", "move position 1 to position 3", "reverse positions 4 through 7" };
        private string password = "abcdefgh";
        private string unpassword = "fbgdceah";

        private bool _reverse = true;
        //private string[] _instructions = new[] { "swap position 4 with position 0 ", "swap letter d with letter b ", "reverse positions 0 through 4 ", "rotate left 1 ", "move position 1 to position 4 ", "move position 3 to position 0 ", "rotate based on position of letter b ", "rotate based on position of letter d" };
        //private string password = "abcde";
        //private string unpassword = "decab";



        public void Solve()
        {
            var instructions = _instructions;
            var newpass = password.ToCharArray();
            if (_reverse)
            {
                instructions = _instructions.Reverse().ToArray();
                newpass = unpassword.ToCharArray();
            }

            foreach (var instruction in instructions)
            {
                var iparts = instruction.Split();
                string pre = new string(newpass);
                if (_reverse)
                {
                    if (iparts[0] == "swap" && iparts[1] == "position") // swap position X with position Y
                    {
                        int Xpos = Convert.ToInt32(iparts[2]);
                        int Ypos = Convert.ToInt32(iparts[5]);
                        char tmp;
                        tmp = newpass[Xpos];
                        newpass[Xpos] = newpass[Ypos];
                        newpass[Ypos] = tmp;
                    }
                    else if (iparts[0] == "swap" && iparts[1] == "letter") // swap letter X with letter Y
                    {
                        for (int i = 0; i < newpass.Length; i++)
                        {
                            if (newpass[i] == iparts[2][0]) newpass[i] = iparts[5][0];
                            else if (newpass[i] == iparts[5][0]) newpass[i] = iparts[2][0];
                        }
                    }
                    else if (iparts[0] == "rotate" && iparts[1] == "based") //rotate based on position of letter X
                    {
                        string nwp = new string(newpass);
                        int newoffset = nwp.IndexOf(iparts[6]);

                        newoffset = newoffset / 2 + (newoffset % 2 == 1 || newoffset == 0 ? 1 : 5);
                        newpass = String.Concat(nwp, nwp, nwp, nwp, nwp).Substring((newoffset) + password.Length + password.Length, password.Length).ToCharArray();
                    }
                    else if (iparts[0] == "rotate") // rotate left/ right X steps
                    {
                        string nwp = new string(newpass);
                        // rotate left X steps => rotate rotate length-X
                        newpass = String.Concat(nwp, nwp, nwp, nwp).Substring(Convert.ToInt32(iparts[2]) * (iparts[1] == "left" ? -1 : 1) + password.Length, password.Length).ToCharArray();
                    }
                    else if (iparts[0] == "reverse") // reverse positions X through Y
                    {
                        int Xpos = Convert.ToInt32(iparts[2]);
                        int Ypos = Convert.ToInt32(iparts[4]);

                        string nwp = new string(newpass);
                        for (int i = Xpos; i <= Ypos; i++)
                            newpass[i] = nwp[Ypos - (i - Xpos)];
                    }
                    else if (iparts[0] == "move") // move position X to position Y
                    {
                        int Xpos = Convert.ToInt32(iparts[5]);
                        int Ypos = Convert.ToInt32(iparts[2]);
                        string nwp = new string(newpass);
                        char tmp = newpass[Xpos];
                        char[] tmpa = new char[newpass.Length - 1];
                        for (int i = 0; i < newpass.Length; i++)
                        {
                            if (i < Xpos) tmpa[i] = newpass[i];
                            if (i > Xpos) tmpa[i - 1] = newpass[i];
                        }
                        for (int i = 0; i < newpass.Length; i++)
                        {
                            if (i == Ypos) newpass[i] = tmp;
                            if (i < Ypos) newpass[i] = tmpa[i];
                            if (i > Ypos) newpass[i] = tmpa[i - 1];
                        }
                    }
                }
                else
                {
                    if (iparts[0] == "swap" && iparts[1] == "position") // swap position X with position Y
                    {
                        int Xpos = Convert.ToInt32(iparts[2]);
                        int Ypos = Convert.ToInt32(iparts[5]);
                        char tmp;
                        tmp = newpass[Xpos];
                        newpass[Xpos] = newpass[Ypos];
                        newpass[Ypos] = tmp;
                    }
                    else if (iparts[0] == "swap" && iparts[1] == "letter") // swap letter X with letter Y
                    {
                        for (int i = 0; i < newpass.Length; i++)
                        {
                            if (newpass[i] == iparts[2][0]) newpass[i] = iparts[5][0];
                            else if (newpass[i] == iparts[5][0]) newpass[i] = iparts[2][0];
                        }
                    }
                    else if (iparts[0] == "rotate" && iparts[1] == "based") //rotate based on position of letter X
                    {
                        string nwp = new string(newpass);
                        int newoffset = nwp.IndexOf(iparts[6]);
                        newoffset++;
                        if (newoffset > 4) newoffset++;
                        newpass = String.Concat(nwp, nwp, nwp, nwp, nwp).Substring((newoffset * -1) + password.Length + password.Length, password.Length).ToCharArray();
                    }
                    else if (iparts[0] == "rotate") // rotate left/ right X steps
                    {
                        string nwp = new string(newpass);
                        // rotate left X steps => rotate rotate length-X
                        newpass = String.Concat(nwp, nwp, nwp, nwp).Substring(Convert.ToInt32(iparts[2]) * (iparts[1] == "left" ? 1 : -1) + password.Length, password.Length).ToCharArray();
                    }
                    else if (iparts[0] == "reverse") // reverse positions X through Y
                    {
                        int Xpos = Convert.ToInt32(iparts[2]);
                        int Ypos = Convert.ToInt32(iparts[4]);

                        string nwp = new string(newpass);
                        for (int i = Xpos; i <= Ypos; i++)
                            newpass[i] = nwp[Ypos - (i - Xpos)];
                    }
                    else if (iparts[0] == "move") // move position X to position Y
                    {
                        int Xpos = Convert.ToInt32(iparts[2]);
                        int Ypos = Convert.ToInt32(iparts[5]);
                        string nwp = new string(newpass);
                        char tmp = newpass[Xpos];
                        char[] tmpa = new char[newpass.Length - 1];
                        for (int i = 0; i < newpass.Length; i++)
                        {
                            if (i < Xpos) tmpa[i] = newpass[i];
                            if (i > Xpos) tmpa[i - 1] = newpass[i];
                        }
                        for (int i = 0; i < newpass.Length; i++)
                        {
                            if (i == Ypos) newpass[i] = tmp;
                            if (i < Ypos) newpass[i] = tmpa[i];
                            if (i > Ypos) newpass[i] = tmpa[i - 1];
                        }
                    }

                }
                Console.WriteLine(String.Format("{1}   {2} => {0}", new string(newpass), instruction.PadRight(40), pre));
            }
            Console.WriteLine("HASHED PASSWORD IS : " + new string(newpass));
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            new ScrambledLettersAndHash().Solve();
            Console.ReadKey();
        }
    }
}
