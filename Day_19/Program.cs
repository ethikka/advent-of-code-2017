﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day_19
{
    public class Elf
    {
        public int number;
        public Elf RightElf;
        public Elf LeftElf;
    }

    public class WhiteElephant
    {
        private int input = 3018458;
        private int left = 3018458;
        //private int input = 5;
        DateTime startTime = DateTime.Now;

        private Elf SolutionA(Elf currentElf)
        {
            currentElf.LeftElf.LeftElf.RightElf = currentElf;
            currentElf.LeftElf = currentElf.LeftElf.LeftElf;
            return currentElf.LeftElf;
        }

        private Elf zakdoek;

        private Elf SolutionB(Elf currentElf)
        {
            Console.SetCursorPosition(1, 1);
            if ((currentElf.number % 2500) == 0)
              Console.WriteLine(String.Format("{0} / {1} ELFS LEFT => Estimated remaining runtime {2}", left, input, ((DateTime.Now - startTime).TotalSeconds / (input - left) * left * 1.0)));
            if (zakdoek == null)
            {
                zakdoek = currentElf;
                for (int i = 0; i < (left + 1) / 2; i++)
                    zakdoek = zakdoek.LeftElf;
                // exact de andere kant
            }
            zakdoek.LeftElf.RightElf = zakdoek.RightElf;
            zakdoek.RightElf.LeftElf = zakdoek.LeftElf;
            if ((left % 2) != 0)
                zakdoek = zakdoek.LeftElf.LeftElf;
            else
                zakdoek = zakdoek.LeftElf;

            left--;
            return currentElf.LeftElf;
        }

        public void Solve()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Elf lastElf = new Elf() { number = 1 };
            Elf firstElf = lastElf;
            for (int i = 2; i <= input; i++)
            {
                lastElf.LeftElf = new Elf() { number = i, RightElf = lastElf };
                lastElf = lastElf.LeftElf;
            }
            lastElf.LeftElf = firstElf;

            startTime = DateTime.Now;
            var currentElf = firstElf;
            while (currentElf.LeftElf != currentElf)
            {
                //currentElf = SolutionA(currentElf);
                currentElf = SolutionB(currentElf);
            }

            sw.Stop();
            Console.SetCursorPosition(2, 2);
            Console.WriteLine("LAST ELF TO HAVE PRESENTS IS: " + currentElf.number.ToString());
            Console.SetCursorPosition(2, 5);
            Console.WriteLine(sw.ElapsedMilliseconds+"ms");

        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            new WhiteElephant().Solve();
            Console.ReadKey();
        }
    }
}
