﻿using System;
using System.Linq;

namespace Day_16
{
    class Program
    {
        public class DragonChecksum
        {
            private static string CollapsedChecksum(string input)
            {
                char[] newChecksum = new char[input.Length/2];
                for (int cnt = 0; cnt < input.Length/2; cnt ++)
                    newChecksum[cnt] = (input[cnt*2] != input[(cnt*2) + 1]) ? '0' : '1';
                return new string(newChecksum);
            }

            public static string CheckSum(string input, int diskspace)
            {
                string tmp = input;
                while (tmp.Length < diskspace)
                    tmp = String.Concat(tmp, "0", new string(tmp.Reverse().ToArray()).Replace("0","x").Replace("1", "0").Replace("x", "1"));
                tmp = tmp.Substring(0, diskspace);

                string checksum = CollapsedChecksum(tmp);
                while ((checksum.Length % 2) == 0)
                    checksum = CollapsedChecksum(checksum);
                
                return checksum;
            }
        }

        static void Main(string[] args)
        {
            // Test    //Console.WriteLine(DragonChecksum.CheckSum("10000", 20).ToString());
            // Part A  //Console.WriteLine(DragonChecksum.CheckSum("10010000000110000", 272).ToString());
            // Part B
            Console.WriteLine(DragonChecksum.CheckSum("10010000000110000", 35651584).ToString()); 
            Console.ReadKey();
        }
    }
}
