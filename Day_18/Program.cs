﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day_18
{
    class Program
    {
        public class LikeARogue
        {
            private const string testinput = ".^^.^.^^^^";
            private const string input = ".^^^^^.^^^..^^^^^...^.^..^^^.^^....^.^...^^^...^^^^..^...^...^^.^.^.......^..^^...^.^.^^..^^^^^...^.";

            private char SafeGet(string input, int pos)
            {
                if (pos < 0 || pos > input.Length-1) return '.';
                return input[pos];
            }

            private string[] Generate(string input, int rowcount)
            {
                string[] res = new string[rowcount];
                res[0] = input;

                for (int i = 1; i < rowcount; i++)
                {
                    char[] buff = new char[input.Length];
                    for (int cnt = 0; cnt < input.Length; cnt++)
                         buff[cnt] = (SafeGet(res[i - 1], cnt - 1) != SafeGet(res[i - 1], cnt + 1)) ? '^' : '.';
                    res[i] = new string(buff);
                }
                return res;
            }

            public void Solve()
            {
                // var tiles = Generate(testinput, 10) // test input;
                //var tiles = Generate(input, 40);     // A solution
                var tiles = Generate(input, 400000);   // B solution

                int count = 0;
                foreach(var s in tiles)
                {
//                    Console.WriteLine(s);              // Uncomment for a print of the floor
                    count += s.Replace("^", "").Length;
                }
                Console.WriteLine("TOTAL SAFE TILES: " + count.ToString());
            }
        }

        static void Main(string[] args)
        {
            new LikeARogue().Solve();
            Console.ReadKey();
        }
    }
}
