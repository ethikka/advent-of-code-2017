﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day_25
{
    public class Tis11
    {
        #region Instructionsets

        string[] instructionStack =
        {
"cpy a d",
"cpy 9 c",
"cpy 282 b",
"inc d",
"dec b",
"jnz b -2",
"dec c",
"jnz c -5",
"cpy d a",
"jnz 0 0",
"cpy a b",
"cpy 0 a",
"cpy 2 c",
"jnz b 2",
"jnz 1 6",
"dec b",
"dec c",
"jnz c -4",
"inc a",
"jnz 1 -7",
"cpy 2 b",
"jnz c 2",
"jnz 1 4",
"dec b",
"dec c",
"jnz 1 -4",
"jnz 0 0",
"out b",
"jnz a -19",
"jnz 1 -21"
        };
        #endregion

        string[] instructions;
        Dictionary<string, int> _registers = new Dictionary<string, int>();
        int _commandpointer;

        private void ToggleInstruction(int targetinstruction)
        {
            if (targetinstruction >= instructions.Length)
                return;

            var command = instructions[targetinstruction].Split();
            switch (command[0])
            {
                case "jnz": instructions[targetinstruction] = instructions[targetinstruction].Replace("jnz", "cpy"); break;
                case "inc": instructions[targetinstruction] = instructions[targetinstruction].Replace("inc", "dec"); break;
                case "dec": instructions[targetinstruction] = instructions[targetinstruction].Replace("dec", "inc"); break;
                case "cpy": instructions[targetinstruction] = instructions[targetinstruction].Replace("cpy", "jnz"); break;
                case "tgl": instructions[targetinstruction] = instructions[targetinstruction].Replace("tgl", "inc"); break;
            }
        }

        private int GetRegOrVal(string input)
        {
            return ("abcd".Contains(input)) ? _registers[input] : Convert.ToInt32(input);
        }

        private void SafeSetRegister(string destination, int source)
        {
            if (_registers.ContainsKey(destination))
                _registers[destination] = source;
        }

        public void Run()
        {
            int run = 0;

            List<String> stacks = new List<string>();
            instructions = instructionStack;

            while (true)
            {
                _commandpointer = 0;
                _registers["a"] = run;
                _registers["b"] = 0;
                _registers["c"] = 0;
                _registers["d"] = 0;
                string output = "";
                while (_commandpointer < instructions.Length)
                {
                    var command = instructions[_commandpointer].Split();
                    int offset = 1;
                    switch (command[0])
                    {
                        case "jnz":
                            if (GetRegOrVal(command[1]) != 0)
                                offset = GetRegOrVal(command[2]);
                            break;
                        case "inc": _registers[command[1]]++; break;
                        case "dec": _registers[command[1]]--; break;
                        case "cpy": SafeSetRegister(command[2], GetRegOrVal(command[1])); break;
                        case "tgl": ToggleInstruction(_commandpointer + GetRegOrVal(command[1])); break;
                        case "out": output += GetRegOrVal(command[1]).ToString(); break;
                    }
                    _commandpointer += offset;
                    if (output.Length > 13)
                    {
                        if (output.Substring(0, 12).Equals("010101010101"))
                        {
                            Console.WriteLine("LOWEST INT FOR REPEATING SIGNAL: " + run);
                            return;
                        }
                        _commandpointer = 99999;
                        run++;
                    }
                        
                }

            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            new Tis11().Run();
            Console.ReadKey();
        }
    }
}
