﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day_23
{
    public class Tis11
    {
        #region Instructionsets
        string[] instructionStackTest =
        {
"cpy 2 a",
"tgl a",
"tgl a",
"tgl a",
"cpy 1 a",
"dec a",
"dec a" };

        string[] instructionStack =
        {
"cpy a b",
"dec b",
"cpy a d",
"cpy 0 a",
"cpy b c",
"inc a",
"dec c",
"jnz c -2",
"dec d",
"jnz d -5",
"dec b",
"cpy b c",
"cpy c d",
"dec d",
"inc c",
"jnz d -2",
"tgl c",
"cpy -16 c",
"jnz 1 c",
"cpy 99 c",
"jnz 77 d",
"inc a",
"inc d",
"jnz d -2",
"inc c",
"jnz c -5"
        };
        #endregion

        string[] instructions;
        Dictionary<string, int> _registers = new Dictionary<string, int>();
        int _commandpointer;

        private void ToggleInstruction(int targetinstruction)
        {
            if (targetinstruction >= instructions.Length)
                return;

            var command = instructions[targetinstruction].Split();
            switch (command[0])
            {
                case "jnz": instructions[targetinstruction] = instructions[targetinstruction].Replace("jnz", "cpy"); break;
                case "inc": instructions[targetinstruction] = instructions[targetinstruction].Replace("inc", "dec"); break;
                case "dec": instructions[targetinstruction] = instructions[targetinstruction].Replace("dec", "inc"); break;
                case "cpy": instructions[targetinstruction] = instructions[targetinstruction].Replace("cpy", "jnz"); break;
                case "tgl": instructions[targetinstruction] = instructions[targetinstruction].Replace("tgl", "inc"); break;
            }
        }

        private int GetRegOrVal(string input)
        {
            return ("abcd".Contains(input)) ? _registers[input] : Convert.ToInt32(input);
        }

        private void SafeSetRegister(string destination, int source)
        {
            if (_registers.ContainsKey(destination))
                _registers[destination] = source;
        }

        public void Run()
        {
            List<String> stacks = new List<string>();
            instructions = instructionStack;
            _registers["a"] = 7;//12;
            _registers["b"] = 0;
            _registers["c"] = 0; 
            _registers["d"] = 0;

            while (_commandpointer < instructions.Length)
            {
                stacks.Add(String.Format("a:{0} b:{1} c:{2} d:{3}", _registers["a"], _registers["b"], _registers["c"], _registers["d"]));
                var command = instructions[_commandpointer].Split();
                int offset = 1;
                switch (command[0])
                {
                    case "jnz":
                        if (GetRegOrVal(command[1]) != 0)
                            offset = GetRegOrVal(command[2]);
                        break;
                    case "inc": _registers[command[1]]++; break;
                    case "dec": _registers[command[1]]--; break;
                    case "cpy": SafeSetRegister(command[2], GetRegOrVal(command[1])); break;
                    case "tgl": ToggleInstruction(_commandpointer + GetRegOrVal(command[1])); break;
                }
                _commandpointer += offset;
            }
            Console.WriteLine(_registers["a"]);
            System.IO.File.AppendAllLines("registers.output", stacks);                
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            new Tis11().Run();
            Console.ReadKey();
        }
    }
}
