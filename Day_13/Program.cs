﻿using System;
using System.Collections.Generic;

namespace Day_13
{
    public class TerseMaze
    {
        Dictionary<string, int> _mazeSpots = new Dictionary<string, int>();
        bool firstPrintDone = false;
        int xlast, ylast = -1;

        private void PrintAt(int x, int y, char ch)
        {
            Console.SetCursorPosition(x, y);
            Console.Write(ch);
        }

        private void FirstPrint()
        {
            PrintAt(31, 39, 'X');
            foreach (var s in _mazeSpots.Keys) PrintAt(Convert.ToInt32(s.Split()[0]), Convert.ToInt32(s.Split()[1]), (_mazeSpots[s] == -1) ? ' ' : '.');
            firstPrintDone = true;
        }

        public void Print(int x, int y)
        {
            if (!firstPrintDone) FirstPrint();
            if (ylast != -1) PrintAt(xlast, ylast, 'o');
            PrintAt(x, y, 'O');
            xlast = x;
            ylast = y;
        }

        public int this[int x, int y]
        {
            get { if ((x == -1) || (y == -1)) return -1;
                  if (!_mazeSpots.ContainsKey(String.Format("{0} {1}", x, y))) _mazeSpots[String.Format("{0} {1}", x, y)] = ((Convert.ToString(((x * x) + (3 * x) + (2 * x * y) + y + (y * y)) + 1364, 2).Replace("0", "").Length % 2) == 0) ? -2 : -1;
                  return _mazeSpots[String.Format("{0} {1}", x, y)]; }
            set { if (!_mazeSpots.ContainsKey(String.Format("{0} {1}", x, y)) || (value < _mazeSpots[String.Format("{0} {1}", x, y)] || _mazeSpots[String.Format("{0} {1}", x, y)] == -2)) _mazeSpots[String.Format("{0} {1}", x, y)] = value; }
        }
    }

    public class Day_13
    {
        Dictionary<string, int> tries = new Dictionary<string, int>();
        private TerseMaze _maze = new TerseMaze();
        private int shortestPath = 999, targetX = 31, targetY = 39, mazeSize = 80;

        private void AddEntries(int x, int y, int astarstep)
        {
            if (astarstep > shortestPath) return;
            int[,] offset = new int[,] { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };
            for(int xx = 0; xx < 4; xx++) if (_maze[x + offset[xx,0], y + offset[xx, 1]] == -2 || _maze[x + offset[xx, 0], y + offset[xx, 1]] > astarstep) tries[String.Format("{0} {1}", x + offset[xx, 0], y + offset[xx, 1])] = astarstep;
        }

        private void TryMove(int x, int y, int astarstep)
        {
            if (x == targetX && y == targetY) shortestPath = Math.Min(shortestPath, astarstep);
            _maze[x, y] = astarstep;
            AddEntries(x, y, astarstep + 1);
            _maze.Print(x, y);
        }
           
        public string Execute()
        {
            for (int x = 0; x < mazeSize * mazeSize; x++) tries["1 1"] = _maze[x / mazeSize, x % mazeSize];
            tries["1 1"] = 0;
            while (true)
            {
                string best = null;
                foreach (var k in tries.Keys) if (tries[best??k] >= tries[k]) best = k;
                if (shortestPath < tries[best]) return shortestPath.ToString();
                TryMove(Convert.ToInt32(best.Split()[0]), Convert.ToInt32(best.Split()[1]), tries[best]);
                tries.Remove(best);
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var res = new Day_13().Execute();
        }
    }
}
