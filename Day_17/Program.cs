﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day_17
{
    public class Settings
    {
        public static System.Security.Cryptography.MD5CryptoServiceProvider MD5 = new System.Security.Cryptography.MD5CryptoServiceProvider();

        public static int RoomX = 4;
        public static int RoomY = 4;
        public static string Openstring = "bcdef";

        //public static string Input = "ihgpwlah";
        public static string Input = "vwbaicqe";
        public static int LongestPath = -1;
    }

    public class PuzzleItem
    {
        public int position; // (x * RoomX) + y
        public string route;

        public List<PuzzleItem> GetMoves()
        {
            List<PuzzleItem> res = new List<PuzzleItem>();
            if (!IsAtVault())
            {
                string hash = BitConverter.ToString(Settings.MD5.ComputeHash(Encoding.ASCII.GetBytes(String.Concat(Settings.Input, this.route)))).Replace("-", string.Empty).ToLower();
                if (position / Settings.RoomX > 0 && (Settings.Openstring.IndexOf(hash[0]) > -1))
                    res.Add(new PuzzleItem() { position = this.position - Settings.RoomX, route = String.Concat(this.route, "U") });
                if (position / Settings.RoomX < (Settings.RoomX - 1) && (Settings.Openstring.IndexOf(hash[1]) > -1))
                    res.Add(new PuzzleItem() { position = this.position + Settings.RoomX, route = String.Concat(this.route, "D") });
                if (position % Settings.RoomY > 0 && (Settings.Openstring.IndexOf(hash[2]) > -1))
                    res.Add(new PuzzleItem() { position = this.position - 1, route = String.Concat(this.route, "L") });
                if (position % Settings.RoomY < (Settings.RoomY - 1) && (Settings.Openstring.IndexOf(hash[3]) > -1))
                    res.Add(new PuzzleItem() { position = this.position + 1, route = String.Concat(this.route, "R") });
            }
            return res;
        }

        public bool IsAtVault()
        {
            return (((Settings.RoomX-1) * Settings.RoomX) + (Settings.RoomY-1)) == position;
        }
    }

    public class TwoStepsForward
    {
        Queue<PuzzleItem> _queue = new Queue<PuzzleItem>();
        public void Calculate()
        {
            _queue.Enqueue(new PuzzleItem() { position = 0, route = "" }); // initial step
            int count = 0;
            while (_queue.Count > 0)
            {
                var current = _queue.Dequeue();
                if (current.IsAtVault())
                {
                    Settings.LongestPath = Math.Max(Settings.LongestPath, current.route.Length); // solution part B
                    //Console.WriteLine("SOLUTION ROUTE: " + current.route); // solution part A
                    //return;                                                // solution part A
                }

                var moves = current.GetMoves();
                foreach (var move in moves)
                    _queue.Enqueue(move);
            }
            Console.WriteLine("SOLUTION LONGEST PATH: " + Settings.LongestPath.ToString());
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            new TwoStepsForward().Calculate();
            Console.ReadKey();
        }
    }
}
