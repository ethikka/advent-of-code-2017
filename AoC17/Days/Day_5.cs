﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AoC17.Days
{
    public class Day_5 : IDay
    {
        private string input = "abbhdwsy";


        private int cnt = 0;
        private string findInterestingHash(string input)
        {
            var hasher = new MD5CryptoServiceProvider();
            string hash;

            hash = BitConverter.ToString(hasher.ComputeHash(Encoding.ASCII.GetBytes(String.Format("{0}{1}", input, cnt)))).Replace("-", "");
            while (!hash.StartsWith("00000"))
            {
                cnt++;
                hash = BitConverter.ToString(hasher.ComputeHash(Encoding.ASCII.GetBytes(String.Format("{0}{1}", input, cnt)))).Replace("-", "");
            }
            System.Diagnostics.Debug.WriteLine(String.Format("{0} : {1} => {2}", input, cnt, hash));
            cnt++;
            return hash.Substring(5, 2);
        }

        public string Execute()
        {
            char[] res = new char[] {'-', '-', '-', '-', '-', '-', '-', '-'};

            while (new string(res).Contains("-"))
            {
                string rr = findInterestingHash(input);
                switch (rr[0])
                {
                    case '0': if (res[0] == '-') res[0] = rr[1]; break;
                    case '1': if (res[1] == '-') res[1] = rr[1]; break;
                    case '2': if (res[2] == '-') res[2] = rr[1]; break;
                    case '3': if (res[3] == '-') res[3] = rr[1]; break;
                    case '4': if (res[4] == '-') res[4] = rr[1]; break;
                    case '5': if (res[5] == '-') res[5] = rr[1]; break;
                    case '6': if (res[6] == '-') res[6] = rr[1]; break;
                    case '7': if (res[7] == '-') res[7] = rr[1]; break;
                    default: break;
                }
                System.Diagnostics.Debug.WriteLine(">>" + new string(res));
            }

            return new string(res).ToLower();
        }
    }
}
