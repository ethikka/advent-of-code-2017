﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AoC17.Days
{
    public class Day_3 : IDay
    {
        public string Execute()
        {
            int validcount = 0;

            var input = Input.Day3();
            // input.Length / 3 omdat de length van een multi dimensional array het totaal aantal elementen is
            for (int i = 0; i < input.Length/3; i++)
            {
                List<int> list = new List<int> { input[i, 0], input[i, 1], input[i, 2] };
                list.Sort();
                if ((list[0] + list[1]) > list[2])
                    validcount++;
            }
            return validcount.ToString();;
        }
    }

    public class Day_3b : IDay
    {
        public string Execute()
        {
            int validcount = 0;

            var input = Input.Day3();

            // input.Length / 3 omdat de length van een multi dimensional array het totaal aantal elementen is
            for (int i = 0; i < input.Length / 3; i++)
            {
                int tmp = (i * 3);
                List<int> list = new List<int> { input[ tmp % 1914   , tmp / 1914],
                                                 input[(tmp % 1914)+1, tmp / 1914],
                                                 input[(tmp % 1914)+2, tmp / 1914] };
                list.Sort();
                if ((list[0] + list[1]) > list[2])
                    validcount++;
            }
            return validcount.ToString(); ;
        }
    }

}
