﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AoC17.Days
{
    public class Day_1 : IDay
    {
        public class Position
        {
            public int X;
            public int Y;
        }

        enum Facing
        {
            north = 0,
            east,
            south,
            west
        }

        Position _currentPosition = new Position();
        Dictionary<int, Dictionary<int, int>> _visitedpositions = new Dictionary<int, Dictionary<int, int>>();

        private Position Move(Facing heading, int steps)
        {
            int deltaX = 0;
            int deltaY = 0;
            switch (heading)
            {
                case Facing.north: deltaY = steps; break;
                case Facing.east:  deltaX = steps; break;
                case Facing.south: deltaY = -1 * steps; break;
                case Facing.west:  deltaX = -1 * steps; break;
            }

            //bool skip = true;

            //foreach(int x in _currentPosition.X.To(_currentPosition.X + deltaX))
            //    foreach (int y in _currentPosition.Y.To(_currentPosition.Y + deltaY))
            //    {
            //        if (skip)
            //        {
            //            if (!_visitedpositions.ContainsKey(x))
            //                _visitedpositions[x] = new Dictionary<int, int>();

            //            if (_visitedpositions[x].ContainsKey(y))
            //                return new Position() { X = x, Y = y };
            //            _visitedpositions[x].Add(y, 1);
            //        }
            //        skip = false;
            //    }

            _currentPosition.X += deltaX;
            _currentPosition.Y += deltaY;
            return null;
        }

        public string Execute()
        {
            string res = "";

            var _steps = Input.Day1().Split(',');

            int i = 0;
            Facing _look = Facing.north;

            while (i < _steps.Length)
            {
                switch (_steps[i].Trim()[0])
                {
                    case 'L': _look = (Facing)(((int)_look + 3) % 4); break;
                    case 'R': _look = (Facing)(((int)_look + 1) % 4); break;
                }

                var shortcut = Move(_look, Convert.ToInt32(_steps[i].Trim().Substring(1)));
                if (shortcut != null)
                    return String.Format("x:{0} y:{1} distance:{2}", shortcut.X, shortcut.Y, (Math.Abs(shortcut.X) + Math.Abs(shortcut.Y)));
                i++;
            }
            res = String.Format("x:{0} y:{1} distance:{2}", _currentPosition.X, _currentPosition.Y, (Math.Abs(_currentPosition.X)+Math.Abs(_currentPosition.Y)));
            return res;
        }
    }

    public class Day_1b : IDay
    {
        public class Position
        {
            public int X;
            public int Y;
        }

        enum Facing
        {
            north = 0,
            east,
            south,
            west
        }

        Position _currentPosition = new Position();
        List<string> _visitedpositions = new List<string>();

        private Position Move(Facing heading, int steps)
        {
            int deltaX = 0;
            int deltaY = 0;
            switch (heading)
            {
                case Facing.north: deltaY = steps; break;
                case Facing.east: deltaX = steps; break;
                case Facing.south: deltaY = -1 * steps; break;
                case Facing.west: deltaX = -1 * steps; break;
            }

            bool skip = true;

            foreach(int x in _currentPosition.X.To(_currentPosition.X + deltaX))
                foreach (int y in _currentPosition.Y.To(_currentPosition.Y + deltaY))
                {
                    if (!skip)
                    {
                        if (_visitedpositions.Contains(String.Format("{0}#{1}", x, y)))
                            return new Position() { X = x, Y = y };
                        _visitedpositions.Add(String.Format("{0}#{1}", x, y));
                    }
                    skip = false;
                }

            _currentPosition.X += deltaX;
            _currentPosition.Y += deltaY;
            return null;
        }

        public string Execute()
        {
            string res = "";

            var _steps = Input.Day1().Split(',');

            int i = 0;
            Facing _look = Facing.north;

            while (i < _steps.Length)
            {
                switch (_steps[i].Trim()[0])
                {
                    case 'L': _look = (Facing)(((int)_look + 3) % 4); break;
                    case 'R': _look = (Facing)(((int)_look + 1) % 4); break;
                }

                var shortcut = Move(_look, Convert.ToInt32(_steps[i].Trim().Substring(1)));
                if (shortcut != null)
                    return String.Format("x:{0} y:{1} distance:{2}", shortcut.X, shortcut.Y, (Math.Abs(shortcut.X) + Math.Abs(shortcut.Y)));
                i++;
            }
            res = String.Format("x:{0} y:{1} distance:{2}", _currentPosition.X, _currentPosition.Y, (Math.Abs(_currentPosition.X) + Math.Abs(_currentPosition.Y)));
            return res;
        }
    }
}
