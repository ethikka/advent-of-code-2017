﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AoC17.Days
{
    public class Day_4 : IDay
    {
        private string alphabetize(string input)
        {
            return String.Concat(input.OrderBy(c => c));
        }

        private int test(string input)
        {
            var regex = new System.Text.RegularExpressions.Regex(@"(.*)-(\d{3})\[(.{5})\]");
            var res = regex.Match(input);
            int sector = Convert.ToInt32(res.Groups[2].Value);
            string checksum = res.Groups[3].Value;
            string payload = res.Groups[1].Value;

            Dictionary<char, int> ss = new Dictionary<char, int>();
            foreach (char s in payload)
                if (s != '-')
                  ss[s] = !ss.ContainsKey(s) ? 1 : ss[s] + 1;
            SortedDictionary<int, string> oss = new SortedDictionary<int, string>();
            foreach (char s in ss.Keys)
                oss[ss[s]] = (oss.ContainsKey(ss[s]) ? oss[ss[s]] : "") + s.ToString();

            // recreate checksum
            string ownchecksum = "";
            
            for (int ii = oss.Count-1; ii >= 0; ii--)
                ownchecksum += alphabetize(oss.ElementAt(ii).Value);

            return checksum == ownchecksum.Substring(0, 5) ? sector: 0;
        }

        public string Execute()
        {
            int total = 0;

            foreach (var s in Input.Day4())
                total += test(s);

            return total.ToString();
        }
    }

    public class Day_4b : IDay
    {
        class RoomEntry
        {
            public int sector;
            public string payload;
            public string checksum;
        }

        private string alphabetize(string input)
        {
            return String.Concat(input.OrderBy(c => c));
        }

        private List<RoomEntry> validrooms = new List<RoomEntry>();

        private void test(string input)
        {
            var regex = new System.Text.RegularExpressions.Regex(@"(.*)-(\d{3})\[(.{5})\]");
            var res = regex.Match(input);
            var room = new RoomEntry();
            room.sector = Convert.ToInt32(res.Groups[2].Value);
            room.checksum = res.Groups[3].Value;
            room.payload = res.Groups[1].Value;

            Dictionary<char, int> ss = new Dictionary<char, int>();
            foreach (char s in room.payload)
                if (s != '-')
                    ss[s] = !ss.ContainsKey(s) ? 1 : ss[s] + 1;
            SortedDictionary<int, string> oss = new SortedDictionary<int, string>();
            foreach (char s in ss.Keys)
                oss[ss[s]] = (oss.ContainsKey(ss[s]) ? oss[ss[s]] : "") + s.ToString();

            // recreate checksum
            string ownchecksum = "";

            for (int ii = oss.Count - 1; ii >= 0; ii--)
                ownchecksum += alphabetize(oss.ElementAt(ii).Value);

            if (room.checksum == ownchecksum.Substring(0, 5))
                validrooms.Add(room);
        }

        private string decrypt(RoomEntry room)
        {
            string res = "";
            foreach (char c in room.payload)
                if (c == '-')
                    res += ' ';
                else
                    res += (char)((((c - 96) + (room.sector % 26)) % 26) + 96);
            if (res.Contains("storage"))
              return res;
            return "";
        }

        public string Execute()
        {
            List<string> sb = new List<string>();
            foreach (var s in Input.Day4())
                test(s);

            foreach (var r in validrooms)
                sb.Add(String.Format("{0} -> {1}", decrypt(r), r.sector));
            // scan by eye for something :(
            return sb.ToString();
        }
    }

}


/*
not-a-real-room-404[oarel]

ooo
aa
rr
e
l
m
n
t


*/
