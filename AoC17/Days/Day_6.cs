﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoC17.Days
{
    public class Day_6 : IDay
    {
        Dictionary<char, int>[] frequencymap;

        private string[] _input = Input.Day6();

        private void PrepareFrequenceMap()
        {
            frequencymap = new Dictionary<char, int>[_input[0].Length];

            for (int i = 0; i < _input[0].Length; i++)
                frequencymap[i] = new Dictionary<char, int>();
            
        }

        public string Execute()
        {
            PrepareFrequenceMap();
            string res = "";

            foreach(var s in _input)
            {
                for (int i = 0; i < _input[0].Length; i++)
                    if (frequencymap[i].ContainsKey(s[i]))
                        frequencymap[i][s[i]] += 1;
                    else
                        frequencymap[i][s[i]] = 1;
            }

            for (int i = 0; i < _input[0].Length; i++)
            {
                // For day 6 (a)  => int num = 0;
                int num = 999;
                // ----------------

                char ch = '-';
                foreach (var k in frequencymap[i].Keys)
                    // For day 6 (a)  => if (frequencymap[i][k] > num)
                    if (frequencymap[i][k] < num)
                    // ----------------
                    {
                        ch = k;
                        num = frequencymap[i][k];
                    }
                res += ch.ToString();
            }

            return res;
        }
    }
}
