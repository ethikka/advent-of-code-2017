﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoC17.Days
{
    public class Day_9 : IDay
    {
        public Int64 Decompress(string input)
        {
            Int64 count = 0; ;
            int idx = 0;
            List<string> _parts = new List<string>();

            // split in decompession parts
            while (idx < input.Length)
            {
                int startpos = input.IndexOf('(', idx);
                if (startpos != idx && startpos != -1)
                    _parts.Add(input.Substring(idx, startpos - idx));
                if (startpos != -1)
                {
                    int endpos = input.IndexOf(')', startpos);

                    int amnt = Convert.ToInt32(input.Substring(startpos + 1, input.IndexOf('x', startpos) - (startpos + 1)));

                    _parts.Add(input.Substring(startpos, (endpos + amnt) - startpos + 1));
                    idx = endpos + amnt + 1;
                }
                else
                {
                    _parts.Add(input.Substring(idx));
                    idx = input.Length;
                }
            }

            foreach (var s in _parts)
            {
                if (s[0] == '(')
                {
                    int xpos = s.IndexOf('x');
                    int endpos = s.IndexOf(')');
                    int multiplier = Convert.ToInt32(s.Substring(xpos + 1, endpos - xpos - 1));
                    count += (multiplier * Decompress(s.Substring(endpos + 1)));
                }
                else
                    count += s.Length;
            }
            return count;
        }




        public string Execute()
        {
            string input = Input.Day9();
            //string input = "(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN";
            //string input = "X(8x2)(3x3)ABCY";

            return Decompress(input).ToString();
        }
    }
}
