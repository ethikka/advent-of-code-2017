﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AoC17.Days
{
    public class Day_2 : IDay
    {
        public class CurrentPosition
        {
            int[,] keypad = new int[3, 3] {{1, 2, 3},
                                           {4, 5, 6},
                                           {7, 8, 9}};

            private int currentX = 1;
            private int currentY = 1;

            public void Move(char direction)
            {
                switch (direction)
                {
                    case 'L': currentX = Math.Max(--currentX, 0); break;
                    case 'R': currentX = Math.Min(++currentX, 2); break;
                    case 'U': currentY = Math.Max(--currentY, 0); break;
                    case 'D': currentY = Math.Min(++currentY, 2); break;
                }
            }

            public int Number
            {
                get
                {
                    return keypad[currentY,currentX];
                }
            }
        }

        CurrentPosition _curpos = new CurrentPosition();

        public string Execute()
        {
            string res = "";

            var input = Input.Day2();
            foreach (var s in input)
            {
                foreach (char c in s.ToCharArray())
                    _curpos.Move(c);
                res += _curpos.Number.ToString();
            }
            return res;
        }
    }

    public class Day_2b : IDay
    {
        public class CurrentPosition
        {
            char[,] keypad = new char[7, 7] { { '0', '0', '0', '0', '0', '0', '0' },
                                              { '0', '0', '0', '1', '0', '0', '0' },
                                              { '0', '0', '2', '3', '4', '0', '0' },
                                              { '0', '5', '6', '7', '8', '9', '0' },
                                              { '0', '0', 'A', 'B', 'C', '0', '0' },
                                              { '0', '0', '0', 'D', '0', '0', '0' },
                                              { '0', '0', '0', '0', '0', '0', '0' } };

            private int currentX = 3;
            private int currentY = 3;

            public void Move(char direction)
            {
                switch (direction)
                {
                    case 'L': if (keypad[currentY, currentX-1] != '0') currentX--; break;
                    case 'R': if (keypad[currentY, currentX+1] != '0') currentX++; break;
                    case 'U': if (keypad[currentY-1, currentX] != '0') currentY--; break;
                    case 'D': if (keypad[currentY+1, currentX] != '0') currentY++; break;
                }
            }

            public char Number
            {
                get
                {
                    return keypad[currentY, currentX];
                }
            }
        }

        CurrentPosition _curpos = new CurrentPosition();

        public string Execute()
        {
            string res = "";

            var input = Input.Day2();
            foreach (var s in input)
            {
                foreach (char c in s.ToCharArray())
                    _curpos.Move(c);
                res += _curpos.Number;
            }
            return res;
        }
    }

}
