﻿using System;
using System.Collections.Generic;

namespace AoC17.Days
{
    public enum TargetType
    {
        ttBot,
        ttOutput
    }

    public class Target
    {
        public Target(string number, string targetstring)
        {
            this.num = Convert.ToInt32(number);
            this.target = (targetstring.Equals("bot")) ? TargetType.ttBot : TargetType.ttOutput;
        }
        public int num;
        public TargetType target; 
    }

    public class Bot
    {
        public Bot(FactoryController owner, int number) { _owner = owner;
                                                          _number = number; }

        FactoryController _owner;
        private int _number;

        private Target _targetLow;
        public Target TargetLow { get { return _targetLow; }
                                  set { _targetLow = value;
                                        TryPassValues();   }}
        private Target _targetHigh;
        public Target TargetHigh { get { return _targetHigh; }
                                   set { _targetHigh = value;
                                         TryPassValues();    }}

        private SortedList<int, int> holdValues = new SortedList<int, int>();

        public void HandValue(int val)
        {
            holdValues.Add(val, val);
            TryPassValues();
        }

        private void TryPassValues()
        {
            if (holdValues.Count == 2 && _targetHigh != null && _targetLow != null)
            {
// For the part A solution uncomment the next 2 lines and wait for the exception
//                if (holdValues.ContainsKey(17) && holdValues.ContainsKey(61))
//                    throw new Exception(String.Format("Bot {0} holds both 17 and 61 chips", this.Number));
                _owner.Handoff(_targetLow, holdValues.Values[0]);
                _owner.Handoff(_targetHigh, holdValues.Values[1]);
                holdValues.Clear();
            }
        }
    }

    public class FactoryController
    {
        private Dictionary<int, Bot> _bots = new Dictionary<int, Bot>();
        private Dictionary<int, int> _outputs = new Dictionary<int, int>();

        public void HandleCommand(string command)
        {
            var tokens = command.Split();
            switch(tokens[0])
            {
                case "value": this.Handoff(new Target(tokens[5], "bot"), Convert.ToInt32(tokens[1]));
                              break;
                case "bot":   int botnum = Convert.ToInt32(tokens[1]);
                              if (!_bots.ContainsKey(botnum))
                                _bots[botnum] = new Bot(this, botnum);
                              _bots[botnum].TargetLow = new Target(tokens[6], tokens[5]);
                              _bots[botnum].TargetHigh = new Target(tokens[11], tokens[10]);
                              break;
            }
        }

        public void Handoff(Target target, int value)
        {
            switch (target.target)
            {
                case TargetType.ttBot:    if (!_bots.ContainsKey(target.num))
                                            _bots[target.num] = new Bot(this, target.num);
                                          _bots[target.num].HandValue(value);
                                          break;
                case TargetType.ttOutput: _outputs[target.num] = value;
                                          break;
            }
        }

        public int Res()
        {
            return _outputs[0] * _outputs[1] * _outputs[2];
        }
    }


    public class Day_10 : IDay
    {
        public string Execute()
        {
            FactoryController fc = new FactoryController();
            foreach (var s in Input.Day10())
                fc.HandleCommand(s.Trim());

            return fc.Res().ToString();
        }
    }
}
