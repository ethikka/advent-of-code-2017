﻿using System;
using System.Collections.Generic;

namespace AoC17.Days
{
    public class Tis10
    {
        #region Instructionsets
        string[] instructionStackTest =
        {
"cpy 41 a",
"inc a",
"inc a",
"dec a",
"jnz a 2",
"dec a" };

        string[] instructionStack =
        {
"cpy 1 a",
"cpy 1 b",
"cpy 26 d",
"jnz c 2",
"jnz 1 5",
"cpy 7 c",
"inc d",
"dec c",
"jnz c -2",
"cpy a c",
"inc a",
"dec b",
"jnz b -2",
"cpy c b",
"dec d",
"jnz d -6",
"cpy 17 c",
"cpy 18 d",
"inc a",
"dec d",
"jnz d -2",
"dec c",
"jnz c -5"
        };
        #endregion

        Dictionary<string, int> _registers = new Dictionary<string, int>();
        int _commandpointer;

        private int GetRegOrVal(string input)
        {
            return ("abcd".Contains(input)) ? _registers[input] : Convert.ToInt32(input);
        }

        public int Run()
        {
            var instructions = instructionStack;
            _registers["a"] = 0;
            _registers["b"] = 0;
            _registers["c"] = 1; // 0 for the a solution
            _registers["d"] = 0;

            while (_commandpointer < instructions.Length)
            {
                var command = instructions[_commandpointer].Split(' ');
                int offset = 1;
                switch (command[0])
                {
                    case "jnz": if (GetRegOrVal(command[1]) != 0) offset = Convert.ToInt32(command[2]); break;
                    case "inc": _registers[command[1]]++;                                               break;
                    case "dec": _registers[command[1]]--;                                               break;
                    case "cpy": _registers[command[2]] = GetRegOrVal(command[1]);                       break;
                }
                _commandpointer += offset;
            }
            return _registers["a"];
        }
    }

    public class Day_12 : IDay
    {
        public string Execute()
        {
            return new Tis10().Run().ToString();
        }
    }
}
