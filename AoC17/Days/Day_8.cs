﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoC17.Days
{
    public class Day_8 : IDay
    {
        public class LEDDisplay
        {
            int[,] _leds;
            int _x, _y;
            public LEDDisplay(int X, int Y)
            {
                _x = X; _y = Y;
                _leds = new int[X, Y];
            }


            public void command(string command)
            {
                if (command.StartsWith("rect "))
                {
                    // RECTANGLE
                    string[] _p = command.Substring(5).Split('x');

                    for (int _xc = 0; _xc < Convert.ToInt32(_p[0]); _xc++)
                        for (int _yc = 0; _yc < Convert.ToInt32(_p[1]); _yc++)
                            _leds[_xc, _yc] = 1;
                }
                else if (command.StartsWith("rotate row "))
                {
                    // ROTATE ROW  //rotate row y=0 by 4  
                    string[] _p = command.Substring(13).Split(' ');
                    int row = Convert.ToInt32(_p[0]);
                    int offset = Convert.ToInt32(_p[2]);

                    int[] newRow = new int[_x];
                    for (int _cnt = 0; _cnt < _x; _cnt++)
                        newRow[(_cnt + offset) % _x] = _leds[_cnt % _x, row];

                    for (int _cnt = 0; _cnt < _x; _cnt++)
                        _leds[_cnt, row] = newRow[_cnt];
                }
                else if (command.StartsWith("rotate column "))
                {
                    // ROTATE COLUMN
                    string[] _p = command.Substring(16).Split(' ');
                    int column = Convert.ToInt32(_p[0]);
                    int offset = Convert.ToInt32(_p[2]);

                    int[] newColumn = new int[_y];
                    for (int _cnt = 0; _cnt < _y; _cnt++)
                        newColumn[(_cnt + offset) % _y] = _leds[column, _cnt % _y];

                    for (int _cnt = 0; _cnt < _y; _cnt++)
                        _leds[column, _cnt] = newColumn[_cnt];
                }


            }

            public int litPixels()
            {
                int res = 0;
                for (int x = 0; x < _x; x++)
                    for (int y = 0; y < _y; y++)
                        if (_leds[x, y] > 0)
                            res++;
                var bla = this.Debug();
                return res;
            }

            public List<string> Debug()
            {
                List<string> res = new List<string>();

                for (int y = 0; y < _y; y++)
                {
                    string row = "";
                    for (int x = 0; x < _x; x++)
                    {
                        if (_leds[x, y] == 0)
                            row += "-";
                        else
                            row += _leds[x, y].ToString();
                    }
                    res.Add(row);
                }

                return res;
            }
        }


        public string Execute()
        {
            //var instructions = Input.Day8Test();
            //            LEDDisplay _display = new LEDDisplay(7, 3);
            var instructions = Input.Day8();
            LEDDisplay _display = new LEDDisplay(50, 6);
            foreach (var i in instructions)
                _display.command(i);

            return _display.litPixels().ToString();
        }
    }
}
