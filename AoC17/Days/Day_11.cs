﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoC17.Days
{
    public class State
    {
        public static byte[] Gen(int g, int m)
        {
            return new byte[] { (byte)g, (byte)m };
        }
    }

    public class ElementController
    {
        private Byte[] GenerateState(int elem1G, int elem1M, int elem2G, int elem2M, int elem3G, int elem3M, int elem4G, int elem4M)
        {
            int generators =  elem1G + 
                             (elem2G << 2) + 
                            ((numElements == 4) ? (elem3G << 4) : 0) + 
                            ((numElements == 4) ? (elem4G << 6) : 0);
            int microchips =  elem1M + 
                             (elem2M << 2) + 
                            ((numElements == 4) ? (elem3M << 4) : 0) + 
                            ((numElements == 4) ? (elem4M << 6) : 0);
            return State.Gen(generators, microchips);
        }

        public ElementController(int elem1G, int elemt1M, int elem2G, int elemt2M, int elem3G, int elemt3M, int elem4G, int elemt4M)
        {
            if (elem4G == -1)
                numElements = 3;

            start = GenerateState(elem1G, elemt1M, elem2G, elemt2M, elem3G, elemt3M, elem4G, elemt4M);

        }
        byte[] start;
        private int numElements = 4;
        private static int _minSteps = -1;

        public int Solve()
        {
            // find a single sub optimal solution

            // backtrack and bruteforce the rest

            return 0;
        }
    }


    public class Day_11 : IDay
    {
        public string Execute()
        {
            // test
            //return new ElementController(2, 1, 3, 1, -1, -1, -1, -1).Solve().ToString();
            // real input
            return new ElementController(1, 1, 1, 2, 1, 2, 3, 3).Solve().ToString();
        }
    }
}
