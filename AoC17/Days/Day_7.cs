﻿using System;
using System.Collections.Generic;

namespace AoC17.Days
{
    class Day_7 : IDay
    {
        public enum ABBARes
        {
            NoAbba,
            NormalAbba,
            HyperloopAbba
        }

        public class ipv7part
        {
            private string _part;
            public ipv7part(string part)
            {
                _part = part;
            }

            public ABBARes IsAbba()
            {
                for (int i = 1; i < _part.Length - 2; i++)
                    if (_part[i] != _part[i - 1])
                        if (_part.Contains(new string(new char[] { _part[i - 1] , _part[i] , _part[i] , _part[i - 1] })))
                            return this.IsHyperloop() ? ABBARes.HyperloopAbba : ABBARes.NormalAbba;
                return ABBARes.NoAbba;
            }

            public bool IsHyperloop()
            {
                return _part.StartsWith("[") && _part.EndsWith("]");
            }
        }

        public class ipv7
        {
            List<ipv7part> _parts = new List<ipv7part>();
            string _sslPartABA = "";
            string _sslPartBAB = "";
            public ipv7(string part)
            {
                int lasthit = 0;
                for (int i = 0; i < part.Length; i++)
                {
                    switch (part[i])
                    {
                        case '[':
                            _sslPartABA += String.Concat(" ", part.Substring(lasthit, i - lasthit));
                            _parts.Add(new ipv7part(part.Substring(lasthit, i - lasthit)));
                            lasthit = i;
                            break;
                        case ']':
                            _sslPartBAB += String.Concat(" ", part.Substring(lasthit, i - lasthit + 1));
                            _parts.Add(new ipv7part(part.Substring(lasthit, i - lasthit + 1)));
                            lasthit = i + 1;
                            break;
                    }
                }
                _sslPartABA += String.Concat(" ", part.Substring(lasthit));
                _parts.Add(new ipv7part(part.Substring(lasthit)));
                _sslPartABA = _sslPartABA.Trim();
                _sslPartBAB = _sslPartBAB.Trim();
            }

            public bool IsAbba()
            {
                bool res = false;
                foreach (var p in _parts)
                {
                    switch (p.IsAbba())
                    {
                        case ABBARes.HyperloopAbba: return false;
                        case ABBARes.NormalAbba: res |= true; break;
                        case ABBARes.NoAbba: break;
                    }
                }
                return res;
            }

            public bool IsSSL()
            {
                for (int i = 1; i < _sslPartABA.Length - 1; i++)
                    if (_sslPartABA[i-1] == _sslPartABA[i + 1])
                        if (_sslPartABA[i] != _sslPartABA[i - 1])
                            if (_sslPartBAB.Contains(new string(new char[] { _sslPartABA[i] , _sslPartABA[i - 1] , _sslPartABA[i] } )))
                                return true;
                return false;
            }
        }

        public string Execute()
        {
            int cnt = 0;

            foreach (var s in Input.Day7())
            {
                var i = new ipv7(s);
                // Day 7 a
                //if (i.IsAbba()) cnt++;
                // Day 7 b
                if (i.IsSSL()) cnt++;
            }

            return cnt.ToString();
        }
    }
}
