﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AoC17
{
    public interface IDay
    {
        string Execute();
    }

    public static class ExtMeths
    {
        public static IEnumerable<int> To(this int from, int to)
        {
            if (from < to)
            {
                while (from <= to)
                {
                    yield return from++;
                }
            }
            else
            {
                while (from >= to)
                {
                    yield return from--;
                }
            }
        }

    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //txtOutput.Text = new Days.Day_1().Execute();
            //txtOutput.Text = new Days.Day_1b().Execute();
            //txtOutput.Text = new Days.Day_2().Execute();
            //txtOutput.Text = new Days.Day_2b().Execute();
            //txtOutput.Text = new Days.Day_3().Execute();
            //txtOutput.Text = new Days.Day_3b().Execute();
            //txtOutput.Text = new Days.Day_4().Execute();
            //txtOutput.Text = new Days.Day_4b().Execute();
            //txtOutput.Text = new Days.Day_5().Execute();
            //txtOutput.Text = new Days.Day_6().Execute();
            //txtOutput.Text = new Days.Day_7().Execute();
            //txtOutput.Text = new Days.Day_8().Execute();
            //txtOutput.Text = new Days.Day_9().Execute();
            //txtOutput.Text = new Days.Day_10().Execute();
            //txtOutput.Text = new Days.Day_11().Execute();
            //txtOutput.Text = new Days.Day_12().Execute();
            //txtOutput.Text = new Days.Day_13().Execute();
        }
    }
}
